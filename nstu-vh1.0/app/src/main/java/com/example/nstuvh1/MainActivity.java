package com.example.nstuvh1;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;

import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;



public class MainActivity extends AppCompatActivity
{

    private AppBarConfiguration mAppBarConfiguration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    /*public void onClickScheduleButton(View view)
    {
        WebView browser=(WebView)findViewById(R.id.webBrowser);
        browser.loadUrl("https://www.nstu.ru/studies/schedule/schedule_classes/schedule?group=%D0%9F%D0%9C-84/");
    }

    public void onClickNewsButton(View view)
    {
        WebView browser=(WebView)findViewById(R.id.webBrowser);
        browser.loadUrl("https://www.nstu.ru/");
    }*/

    public void onMyButtonClick(View view)
    {
        WebView browser=(WebView)findViewById(R.id.webBrowser);
        switch(view.getId())
        {
            case R.id.news_button:
                browser.loadUrl("https://www.nstu.ru/");
                break;

            case R.id.schedule_button:
                browser.loadUrl("https://www.nstu.ru/studies/schedule/schedule_classes/schedule?group=%D0%9F%D0%9C-84/");
                break;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    public boolean onMyItemClick(MenuItem item)
    {
        Intent intent;
        switch (item.getItemId())
        {
            case R.id.lk_item:
                intent = new Intent(this, LkActivity.class);
                startActivity(intent);
                return true;
            case R.id.Head:
                intent = new Intent(this, DekanActivity.class);
                startActivity(intent);
                return true;
            case R.id.Settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.Raiting:
                intent = new Intent(this, PerformanceActivity.class);
                startActivity(intent);
                return true;
            case R.id.Help:
                intent  = new Intent(this, HelpActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}

