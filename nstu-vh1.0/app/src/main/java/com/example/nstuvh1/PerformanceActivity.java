package com.example.nstuvh1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

public class PerformanceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);
    }

    public void onMyButtonClick(View view) {
        WebView browser = (WebView) findViewById(R.id.webBrowser);
        String path = null;
        Intent intent = new Intent(this, LkEmptyActivity.class);
        switch (view.getId()) {
            case R.id.news_button:
                browser.loadUrl("https://www.nstu.ru/");
                break;

            case R.id.schedule_button:
                browser.loadUrl("https://www.nstu.ru/studies/schedule/schedule_classes/schedule?group=%D0%9F%D0%9C-84/");
                break;

            case R.id.nstulibrary_button:
                path = "http://virtua.library.nstu.ru/";
                intent.putExtra("path", path);
                startActivity(intent);
                break;
        }
    }
}