package com.example.nstuvh1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    private static TextView help_dialog_window;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_help);
        help_dialog_window = findViewById(R.id.help_dialog_window);
        //help_dialog_window.setText(findViewById(R.id.name_surname));

       //DrawerLayout drawer = findViewById(R.id.drawer_help);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        AppBarConfiguration mAppBarConfiguration = new AppBarConfiguration.Builder()
//                .setDrawerLayout(drawer)
//                .build();

    }

    public void onClickFeedback(View view)
    {
        setContentView(R.layout.activity_feedback);
    }

    public void onClickScheduleButton(View view)
    {
        Intent intent  = new Intent(".MainActivity");
        startActivity(intent);
        WebView browser=(WebView)findViewById(R.id.webBrowser);
        browser.loadUrl("https://www.nstu.ru/studies/schedule/schedule_classes/schedule?group=%D0%9F%D0%9C-84/");
    }

    public void onClickNewsButton(View view)
    {
        WebView browser=(WebView)findViewById(R.id.webBrowser);
        browser.loadUrl("https://www.nstu.ru/");
    }

    public void onClickMailButton(View view)
    {

    }
}